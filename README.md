## Log Parser
Parsing log file using Spring (Boot, Batch, Data).

#### Dependencies
1. MySQL Database `wallethub`
2. Java 1.8

#### How to use it
`java -jar parser.jar --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 --accesslog=C:\Users\pc\Desktop\access.log`

, will find all IPs that made requests more than threshold between the time

`java -jar parser.jar --count=192.168.102.136 --accesslog=C:\Users\pc\Desktop\access.log` 

, will get you all requests made by the ip

#### For getting help
`java -jar parser.jar --help`
