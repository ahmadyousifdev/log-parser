package com.ef;

import static com.ef.constant.ParserConst.DATE_PATTERN;
import static org.apache.commons.lang3.time.DateUtils.parseDate;
import static org.hamcrest.CoreMatchers.equalTo;

import com.ef.enums.Argument;
import com.ef.enums.Duration;
import com.ef.query.Queries;
import java.io.File;
import java.text.ParseException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class ParserTest {

    private static final long THRESHOLD = 2L;
    private static final String IP = "192.168.234.82";
    private static final String DUMMY_STING = "test";
    private static final String LOG_FILE = "access.log";
    private static final String FROM_DATE = "2017-01-01 00:00:11.763";
    private static final String TO_DATE = "2017-01-01 00:00:12.763";

    private File file;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() {
        file = new File(ParserTest.class.getClassLoader()
                .getResource(LOG_FILE).getFile());
    }

    @Rule
    public final ErrorCollector collector = new ErrorCollector();

    @Test
    public void givenParserWhenDurationDailyThenExecute() throws Exception {
        // Arrange
        JobParameters jobParameters = createJobParametersForParserWhenDurationDaily();

        // Act
        JobLauncher launcher = context.getBean(JobLauncher.class);
        JobExecution result = launcher.run(context.getBean(Job.class), jobParameters);

        // Assert
        assertCommon(result);
    }

    @Test
    public void givenParserWhenIpFoundThenReturnsCount() throws Exception {
        // Arrange
        JobParameters jobParameters = createJobParametersForParserWhenIp(IP);

        // Act
        JobLauncher launcher = context.getBean(JobLauncher.class);
        JobExecution result = launcher.run(context.getBean(Job.class), jobParameters);
        Long actual = jdbcTemplate.queryForObject(Queries.SELECT_LOG_ITEM_BY_IP, new Object[]{IP}, Long.class);

        // Assert
        assertCommon(result);
        collector.checkThat(actual, equalTo(6L));
    }

    @Test
    public void givenParserWhenIpNotFoundThenReturnsZero() throws Exception {
        // Arrange
        JobParameters jobParameters = createJobParametersForParserWhenIp(DUMMY_STING);

        // Act
        JobLauncher launcher = context.getBean(JobLauncher.class);
        JobExecution result = launcher.run(context.getBean(Job.class), jobParameters);
        Long actual = jdbcTemplate.queryForObject(Queries.SELECT_LOG_ITEM_BY_IP, new Object[]{DUMMY_STING}, Long.class);

        // Assert
        assertCommon(result);
        collector.checkThat(actual, equalTo(0L));

    }

    private void assertCommon(JobExecution result) {
        result.getStepExecutions().forEach(s -> {
            collector.checkThat(result.getStatus(), equalTo(BatchStatus.COMPLETED));
        });
    }

    private JobParameters createJobParametersForParserWhenIp(String ip) {
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString(Argument.count.name(), ip);
        assertCreateJobParametersCommon(jobParametersBuilder);
        return jobParametersBuilder.toJobParameters();
    }

    private JobParameters createJobParametersForParserWhenDurationDaily() throws ParseException {
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addDate(Argument.startDate.name(), parseDate(FROM_DATE, DATE_PATTERN));
        jobParametersBuilder.addDate(Argument.toDate.name(), parseDate(TO_DATE, DATE_PATTERN));
        jobParametersBuilder.addString(Argument.duration.name(), Duration.DAILY.name());
        jobParametersBuilder.addLong(Argument.threshold.name(), THRESHOLD);
        assertCreateJobParametersCommon(jobParametersBuilder);
        return jobParametersBuilder.toJobParameters();
    }

    private void assertCreateJobParametersCommon(
            JobParametersBuilder jobParametersBuilder) {
        jobParametersBuilder.addString(Argument.accesslog.name(), file.getAbsolutePath());
    }
}
