package com.ef.processor;

import static com.ef.constant.ParserConst.REASON_MESSAGE;

import com.ef.entity.BanItem;
import com.ef.entity.LogBanItem;
import com.ef.enums.Argument;
import com.sun.javafx.binding.StringFormatter;
import java.util.Map;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class BanItemProcessor implements ItemProcessor<LogBanItem, BanItem> {

    @Value("#{jobParameters}")
    private Map<String, ?> jobParameters;

    @Override
    public BanItem process(final LogBanItem log) {
        BanItem item = new BanItem();
        item.setIp(log.getIp());
        item.setReason(StringFormatter.format(REASON_MESSAGE, log.getTotal(),
                jobParameters.get(Argument.threshold.name())).getValue());
        return item;
    }
}
