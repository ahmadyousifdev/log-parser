package com.ef.configuration;

import static com.ef.constant.ParserConst.COUNT;

import com.ef.entity.BanItem;
import com.ef.entity.LogBanItem;
import com.ef.enums.Argument;
import com.ef.query.Queries;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@Slf4j
public class LogOperationConfiguration {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Bean("logOperationStep")
    public Step logOperationStep() {
        return stepBuilderFactory.get("logOperationStep")
                .<LogBanItem, BanItem>tasklet((StepContribution contribution, ChunkContext chunkContext) -> {
                    String ip = (String) chunkContext.getStepContext().getJobParameters().get(Argument.count.name());
                    Long count = jdbcTemplate
                            .queryForObject(Queries.SELECT_LOG_ITEM_BY_IP, new Object[]{ip}, Long.class);
                    System.out.println(String.format(COUNT, count));
                    log.info(String.format(COUNT, count));
                    return RepeatStatus.FINISHED;
                }).build();
    }
}
