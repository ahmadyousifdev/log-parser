package com.ef;

import static com.ef.constant.ParserConst.HELP;

import com.ef.console.ConsoleWriter;
import com.ef.exception.ParserException;
import com.ef.util.ArgumentUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EnableBatchProcessing
@Slf4j
public class Application {

    public static void main(String args[]) throws BeansException, JobExecutionAlreadyRunningException,
            JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
        JobParameters jobParameters = null;
        try {
            jobParameters = ArgumentUtils.getJobParameters(args);
        } catch (ParserException e) {
            if (e.getMessage().contains(HELP)) {
                ConsoleWriter.printHelp();
            } else {
                log.error(e.getMessage());
            }
            System.exit(0);
        }
        ApplicationContext context = SpringApplication.run(Application.class, args);
        JobLauncher jobLauncher = context.getBean(JobLauncher.class);
        jobLauncher.run(context.getBean(Job.class), jobParameters);
    }
}
