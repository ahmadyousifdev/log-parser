package com.ef.console;

import static com.ef.constant.ParserConst.HELP_MESSAGE;
import static com.ef.constant.ParserConst.SUPPRESS_DEFAULT_CONSTRUCTOR_MESSAGE;

public final class ConsoleWriter {

    private ConsoleWriter() {
        throw new AssertionError(SUPPRESS_DEFAULT_CONSTRUCTOR_MESSAGE);
    }

    public static void printHelp() {
        System.out.println(HELP_MESSAGE);
    }
}
