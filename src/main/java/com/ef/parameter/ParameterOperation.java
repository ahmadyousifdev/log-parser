package com.ef.parameter;

import static com.ef.constant.ParserConst.DASH;
import static com.ef.constant.ParserConst.DATE_CONSOLE_PATTERN;
import static com.ef.constant.ParserConst.EQUALS;
import static com.ef.constant.ParserConst.HELP;
import static com.ef.constant.ParserConst.INVALID_NUMBER_OF_ARGUMENT;
import static com.ef.constant.ParserConst.INVALID_OPTION_MESSAGE;
import static com.ef.constant.ParserConst.PLEASE_ENTER_AN_ARGUMENTS_MESSAGE;
import static org.apache.commons.lang3.time.DateUtils.addDays;
import static org.apache.commons.lang3.time.DateUtils.addHours;
import static org.apache.commons.lang3.time.DateUtils.parseDate;

import com.ef.constant.ParserArgumentCount;
import com.ef.enums.Argument;
import com.ef.enums.Duration;
import com.ef.exception.ParserException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ParameterOperation {

    public static Map<Argument, Object> getParameters(String[] args) throws ParserException {
        validateArgs(args);
        return getParams(args);
    }

    private static Map<Argument, Object> getParams(String[] args) throws ParserException {
        Map<Argument, Object> params = new HashMap<>();
        Arrays.asList(args).forEach(param -> {
            String[] split = param.split(EQUALS);
            validateParam(split[0]);
            if (split[0].contains(Argument.startDate.name())) {
                try {
                    params.put(Argument.startDate, parseDate(split[1], DATE_CONSOLE_PATTERN));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (split[0].contains(Argument.duration.name())) {
                params.put(Argument.duration, split[1]);
            } else if (split[0].contains(Argument.threshold.name())) {
                params.put(Argument.threshold, Long.parseLong(split[1]));
            } else if (split[0].contains(Argument.accesslog.name())) {
                params.put(Argument.accesslog, split[1]);
            } else if (split[0].contains(Argument.count.name())) {
                params.put(Argument.count, split[1]);
            }
        });

        Date toDateString = null;
        if (Duration.DAILY.name().equalsIgnoreCase((String) params.get(Argument.duration))) {
            toDateString = addDays((Date) params.get(Argument.startDate), 1);
        } else if (Duration.HOURLY.name().equalsIgnoreCase((String) params.get(Argument.duration))) {
            toDateString = addHours((Date) params.get(Argument.startDate), 1);
        }
        if (toDateString != null) {
            params.put(Argument.toDate, toDateString);
        }
        validateParamSize(params);
        ParserArgumentCount.setCount(params.size());
        return params;
    }

    private static void validateParamSize(Map<Argument, Object> params) throws ParserException {
        if (params.size() != 2 && params.size() != 5) {
            throw new ParserException(INVALID_NUMBER_OF_ARGUMENT);
        }
    }

    private static void validateParam(String param) {
        if (param.lastIndexOf(DASH) != 1) {
            throw new IllegalArgumentException(INVALID_OPTION_MESSAGE);
        }
    }

    private static void validateArgs(String[] args) throws ParserException {
        if (args.length <= 0) {
            throw new IllegalArgumentException(PLEASE_ENTER_AN_ARGUMENTS_MESSAGE);
        } else if (args.length <= 1 && args[0].contains(HELP)) {
            throw new ParserException(HELP);
        }
    }
}
