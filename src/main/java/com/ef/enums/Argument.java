package com.ef.enums;

public enum Argument {

    startDate, toDate, duration, threshold, accesslog, count
}
