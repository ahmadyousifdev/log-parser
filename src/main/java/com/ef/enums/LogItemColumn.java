package com.ef.enums;

public enum LogItemColumn {

    TIME, IP, VERSION, STATUS, BROWSER
}
