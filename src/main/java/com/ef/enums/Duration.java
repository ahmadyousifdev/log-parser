package com.ef.enums;

public enum Duration {

    HOURLY, DAILY
}
